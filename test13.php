<?php
function show_Spanish($n, $m)
{
    return("The number $n is called $m in Spanish");
}

function map_Spanish($n, $m)
{
    return(array($n => $m));
}

$a = array(1);
$b = array("6", "7", "8", "9", "10", "11");

$c = array_map("show_Spanish", $a, $b);
print_r($c);

// $d = array_map("map_Spanish", $a , $b);
// print_r($d);
?>