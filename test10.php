<div class="form-group row">
    <label class="col-md-3 col-form-label">Inline Checkboxes</label>
    <div class="col-md-9 col-form-label">
        <div class="form-check form-check-inline mr-1">
            <input class="form-check-input" id="inline-checkbox1" type="checkbox" value="check1">
            <label class="form-check-label" for="inline-checkbox1">One</label>
        </div>
        <div class="form-check form-check-inline mr-1">
            <input class="form-check-input" id="inline-checkbox2" type="checkbox" value="check2">
            <label class="form-check-label" for="inline-checkbox2">Two</label>
        </div>
        <div class="form-check form-check-inline mr-1">
            <input class="form-check-input" id="inline-checkbox3" type="checkbox" value="check3">
            <label class="form-check-label" for="inline-checkbox3">Three</label>
        </div>
    </div>
</div>




<div class="form-group row">
    <label class="col-md-3 col-form-label">Checkboxes</label>
    <div class="col-md-9 col-form-label">
        <div class="form-check checkbox">
            <input class="form-check-input" id="check1" type="checkbox" value="">
            <label class="form-check-label" for="check1">Option 1</label>
        </div>
        <div class="form-check checkbox">
            <input class="form-check-input" id="check2" type="checkbox" value="">
            <label class="form-check-label" for="check2">Option 2</label>
        </div>
        <div class="form-check checkbox">
            <input class="form-check-input" id="check3" type="checkbox" value="">
            <label class="form-check-label" for="check3">Option 3</label>
        </div>
    </div>
</div>