< !DOCTYPE html>
  <html>

  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
  </head>
  <style>

input[type=radio] {
  display: none;
}

.radio {
  box-sizing: border-box;
  -webkit-transition: background-color 0.2s linear;
  transition: background-color 0.2s linear;
  position: relative;
  display: inline-block;
  margin: 0 20px 8px 0;
  padding: 12px 12px 12px 42px;
  border-radius: 8px;
  background-color: #f6f7f8;
  vertical-align: middle;
  cursor: pointer;
}
.radio:hover {
  background-color: #e2edd7;
}
.radio:hover:after {
  border-color: #53b300;
}
.radio:after {
  -webkit-transition: border-color 0.2s linear;
  transition: border-color 0.2s linear;
  position: absolute;
  top: 50%;
  left: 15px;
  display: block;
  margin-top: -10px;
  width: 16px;
  height: 16px;
  border: 2px solid #bbb;
  border-radius: 6px;
  content: '';
}

.radio:before {
  -webkit-transition: opacity 0.2s linear;
  transition: opacity 0.2s linear;
  position: absolute;
  top: 50%;
  left: 20px;
  display: block;
  margin-top: -5px;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: #53b300;
  content: '';
  opacity: 0;
}
input[type=radio]:checked + .radio:before {
  opacity: 1;
}



</style>

  <body>
    <form action="">
      <section>
        <h2>radio</h2>
        <input type="radio" name="hoge" value="高坂さん" checked id="radio01"/> <label for="radio01" class="radio"></label>
        <input type="radio" name="hoge" value="南さん" id="radio02" /><label for="radio02" class="radio">南さん</label>
        <input type="radio" name="hoge" value="園田さん" id="radio03" /><label for="radio03" class="radio">園田さん</label>
      </section>
      <section>
        <h2>checkbox</h2>
        <input type="checkbox" name="piyo" value="西木野さん" checked id="checkbox01" /> <label for="checkbox01" class="checkbox">西木野さん</label>
        <input type="checkbox" name="piyo" value="小泉さん" id="checkbox02" /> <label for="checkbox02" class="checkbox">小泉さん</label>
        <input type="checkbox" name="piyo" value="星空さん" id="checkbox03" /> <label for="checkbox03" class="checkbox">星空さん</label>
      </section>
    </form>
  </body>

  </html>