<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contents extends MY_Controller
{
    private $store_model;
    private $available_language_list_model;
    private $available_language_ids;

    public function __construct()
    {
        parent::__construct();
        $this->api_key_auth();
        $this->load->model('last_update_model');
        $this->load->model('m_store_model');
        $this->load->model('m_region_model');
        $this->load->model('m_available_language_list_model');
        $this->load->model('m_products_model');
        $this->load->model('m_products_translate_model');
        $this->load->model('m_region_products_list_model');
        $this->load->model('m_dr_type_translate_model');
        $this->load->model('m_top_category_model');
        $this->load->model('m_top_category_translate_model');
        $this->load->model('m_skin_category_model');
        $this->load->model('m_skin_category_translate_model');
        $this->load->model('m_line_model');
        $this->load->model('m_line_translate_model');
        $this->load->model('m_region_life_style_model');
        $this->load->model('m_region_ideal_single_model');
        $this->load->model('m_region_ideal_multi_model');
        $this->load->model('m_store_best_seller_model');
        $this->load->model('m_region_best_seller_model');
        $this->load->model('m_sensor_store_basic_model');
        $this->load->model('m_sensor_store_detail_model');
    }

    public function get_contents_post()
    {
        $store_id = $this->post('store_id');
        $cms_last_update = $this->post('cms_last_update');
        $server_last_update = $this->last_update_model->get_last_update_datetime();

        if(!$store_id || is_null($store_id)) {
            $this->response(['message'=>'Incorrect Parameter value.'], REST_Controller::HTTP_FORBIDDEN);
        }

        $this->store_model = $this->m_store_model->get_by_id($store_id, STATUS_ENABLE);

        if(!$this->store_model) {
            $this->response(['message'=>'Incorrect Parameter value.'], REST_Controller::HTTP_FORBIDDEN);
        }

        if($server_last_update <= $cms_last_update) {
            $this->response(['message'=>'No any updated values.'], REST_Controller::HTTP_OK);
        }

        $this->available_language_list_model = $this->m_available_language_list_model->get_by_columns(['region_id'=>$this->store_model->region_id], STATUS_ENABLE);
        $this->available_language_ids = array_column($this->available_language_list_model, 'id');
        $region_model = $this->m_region_model->get_by_id($this->store_model->region_id);

        $output = [
            'store_id' => (int)$this->store_model->id,
            'store_name' => $this->store_model->store_name,
            'default_language_code' => $region_model->code,
            'data' => [
                'products' => $this->get_products(),
                'master_data' => $this->get_master(),
                'condition_data' => $this->get_condition(),
                'sensor_data' => $this->get_sensor(),
            ],
            'cms_last_update' => $server_last_update,
            'server_datetime' => date('Y-m-d H:i:s'),
        ];

        $this->response($output, REST_Controller::HTTP_OK);
    }

    private function get_products()
    {
        $region_products_list_model = $this->m_region_products_list_model->get_by_columns(['region_id'=>$this->store_model->region_id]);
        $region_products_list_ids = array_column($region_products_list_model, 'product_id');
        $products_model = $this->m_products_model->get_by_id($region_products_list_ids, STATUS_ENABLE);
        $products_ids = array_column($products_model, 'id');
        $products_translate_model = $this->m_products_translate_model->get_by_columns(['product_id' => $products_ids, 'language_id' => $this->available_language_ids]);

        foreach ($products_model as $product) {
            $region_products_key = array_search($product->id, array_column($region_products_list_model, 'product_id'));
            $language_products_keys = array_keys(array_column($products_translate_model, 'product_id'), $product->id);
            $products_translate = array_map(function($k) use ($products_translate_model){return $products_translate_model[$k];}, $language_products_keys);

            $tmp = [
                'product_id' => (int)$product->id,
                'line_id' => (int)$product->line_id,
                'use_timing_type' => (int)$product->use_timing_type,
                'quasi_drug_flg' => (int)$product->quasi_drug_flg,
                'dr_type_id' => (int)$product->dr_type_id,
                'product_file' => $region_products_list_model[$region_products_key]->product_file,
                'product_file_checksum' => $region_products_list_model[$region_products_key]->product_file_checksum,
            ];

            foreach ($this->available_language_list_model as $available_language_list) {
                $language_key = array_search($available_language_list->id, array_column($products_translate, 'language_id'));
                $language[] = [
                    'language_code' => $available_language_list->code,
                    'product_name' => $language_key === false ? '' : $products_translate[$language_key]->product_name,
                    'product_content' => $language_key === false ? '' : $products_translate[$language_key]->product_content,
                    'repletion_text' => [
                        $language_key === false ? '' : $products_translate[$language_key]->repletion_text1,
                        $language_key === false ? '' : $products_translate[$language_key]->repletion_text2,
                    ],
                ];
            }

            $tmp['language'] = $language;
            $output[] = $tmp;
            unset($tmp, $language);
        }

        return isset($output) ? $output : [];
    }

    private function get_master()
    {
        foreach ($this->available_language_list_model as $available_language_list) {
            $ms_language[] = [
                'language_code' => $available_language_list->code,
                'language_name' => $available_language_list->app_show_name,
            ];
        }

        $dr_type_translate_model = $this->m_dr_type_translate_model->get_by_columns(['language_id' => $this->available_language_ids]);
        for ($i=1;$i<=2;$i++) {
            $dr_type_translate_keys = array_keys(array_column($dr_type_translate_model, 'dr_type_id'), $i);
            $dr_type_translate = array_map(function($k) use ($dr_type_translate_model){return $dr_type_translate_model[$k];}, $dr_type_translate_keys);
            foreach ($this->available_language_list_model as $available_language_list) {
                $language_key = array_search($available_language_list->id, array_column($dr_type_translate, 'language_id'));
                $language[] = [
                    'language_code' => $available_language_list->code,
                    'dr_type_name' => $language_key === false ? '' : $dr_type_translate[$language_key]->dr_type_name,
                    'dr_type_content' => $language_key === false ? '' : $dr_type_translate[$language_key]->dr_type_content,
                ];
            }
            $dr_type[] = [
                'dr_type_id' => $i,
                'language' => $language
            ];
            unset($language);
        }

        $top_category_model = $this->m_top_category_model->get_all(STATUS_ENABLE);
        $top_category_keys = array_column($top_category_model, 'id');
        $top_category_translate_model = $this->m_top_category_translate_model->get_by_columns(['top_category_id'=>$top_category_keys]);
        foreach ($top_category_model as $top_category_row) {
            $top_category_translate_keys = array_keys(array_column($top_category_translate_model, 'top_category_id'), $top_category_row->id);
            $top_category_translate = array_map(function($k) use ($top_category_translate_model){return $top_category_translate_model[$k];}, $top_category_translate_keys);
            foreach ($this->available_language_list_model as $available_language_list) {
                $language_key = array_search($available_language_list->id, array_column($top_category_translate, 'language_id'));
                $language[] = [
                    'language_code' => $available_language_list->code,
                    'top_category_name' => $language_key === false ? '' : $top_category_translate[$language_key]->top_category_name,
                    'top_category_content' => $language_key === false ? '' : $top_category_translate[$language_key]->top_category_content,
                ];
            }
            $top_category[] = [
                'top_category_id' => (int)$top_category_row->id,
                'language' => $language
            ];
            unset($language);
        }

        $skin_category_model = $this->m_skin_category_model->get_all(STATUS_ENABLE);
        $skin_category_keys = array_column($skin_category_model, 'id');
        $skin_category_translate_model = $this->m_skin_category_translate_model->get_by_columns(['skin_category_id'=>$skin_category_keys]);
        foreach ($skin_category_model as $skin_category_row) {
            $skin_category_translate_keys = array_keys(array_column($skin_category_translate_model, 'skin_category_id'), $skin_category_row->id);
            $skin_category_translate = array_map(function($k) use ($skin_category_translate_model){return $skin_category_translate_model[$k];}, $skin_category_translate_keys);
            foreach ($this->available_language_list_model as $available_language_list) {
                $language_key = array_search($available_language_list->id, array_column($skin_category_translate, 'language_id'));
                $language[] = [
                    'language_code' => $available_language_list->code,
                    'skin_category_name' => $language_key === false ? '' : $skin_category_translate[$language_key]->skin_category_name,
                    'skin_category_content' => $language_key === false ? '' : $skin_category_translate[$language_key]->skin_category_content,
                ];
            }
            $skin_category[] = [
                'skin_category_id' => (int)$skin_category_row->id,
                'top_category_id' => (int)$skin_category_row->top_category_id,
                'language' => $language
            ];
            unset($language);
        }

        $line_model = $this->m_line_model->get_all(STATUS_ENABLE);
        $line_keys = array_column($line_model, 'id');
        $line_translate_model = $this->m_line_translate_model->get_by_columns(['line_id'=>$line_keys]);
        foreach ($line_model as $line_row) {
            $line_translate_keys = array_keys(array_column($line_translate_model, 'line_id'), $line_row->id);
            $line_translate = array_map(function($k) use ($line_translate_model){return $line_translate_model[$k];}, $line_translate_keys);
            foreach ($this->available_language_list_model as $available_language_list) {
                $language_key = array_search($available_language_list->id, array_column($line_translate, 'language_id'));
                $language[] = [
                    'language_code' => $available_language_list->code,
                    'line_name' => $language_key === false ? '' : $line_translate[$language_key]->line_name,
                ];
            }
            $line[] = [
                'line_id' => (int)$line_row->id,
                'language' => $language
            ];
            unset($language);
        }

        $output = [
            'language' => $ms_language ? $ms_language : [],
            'dr_type' => $dr_type ? $dr_type : [],
            'top_category' => $top_category ? $top_category : [],
            'skin_category' => $skin_category ? $skin_category : [],
            'line' => $line ? $line : [],
        ];

        return $output;
    }

    private function get_condition()
    {
        $region_life_style_model = $this->m_region_life_style_model->get_by_columns(['region_id'=>$this->store_model->region_id], ['priority, update_date'=>'desc']);
        foreach ($region_life_style_model as $region_life_style_row) {
            $region_life_style[$region_life_style_row->skin_category_id][] = (int)$region_life_style_row->product_id;
        }
        foreach ($region_life_style as $key => $value) {
            $life_style_beauty[] = [
                'skin_category_id' => $key,
                'product_id' => $value
            ];
        }

        $region_ideal_single_model = $this->m_region_ideal_single_model->get_by_columns(['region_id'=>$this->store_model->region_id], ['priority, update_date'=>'desc']);
        foreach ($region_ideal_single_model as $region_ideal_single_row) {
            $region_ideal_single[$region_ideal_single_row->skin_category_id][$region_ideal_single_row->age_id][] = (int)$region_ideal_single_row->product_id;
        }
        foreach ($region_ideal_single as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $ideal_beauty_single[] = [
                    'skin_category_id' => $key,
                    'age_id' => $key2,
                    'product_id' => $value2
                ];
            }
        }

        $region_ideal_multi_model = $this->m_region_ideal_multi_model->get_by_columns(['region_id'=>$this->store_model->region_id], ['priority, update_date'=>'desc']);
        foreach ($region_ideal_multi_model as $region_ideal_multi_row) {
            $region_ideal_multi[$region_ideal_multi_row->select_category_type][$region_ideal_multi_row->age_id][] = (int)$region_ideal_multi_row->product_id;
        }
        foreach ($region_ideal_multi as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $ideal_beauty_multi[] = [
                    'select_category_type' => $key,
                    'age_id' => $key2,
                    'product_id' => $value2
                ];
            }
        }

        $best_seller_model = $this->m_store_best_seller_model->get_by_columns(['store_id'=>$this->store_model->id], ['best_seller_rank, update_date'=>'desc']);
        if(!$best_seller_model) {
            $best_seller_model = $this->m_region_best_seller_model->get_by_columns(['region_id'=>$this->store_model->region_id], ['best_seller_rank, update_date'=>'desc']);
        }
        foreach ($best_seller_model as $best_seller_row) {
            $product_id[] = (int)$best_seller_row->product_id;
        }
        $best_seller = [
            'product_id' => $product_id
        ];

        $output = [
            'life_style_beauty' => $life_style_beauty ? $life_style_beauty : [],
            'ideal_beauty_single' => $ideal_beauty_single ? $ideal_beauty_single : [],
            'ideal_beauty_multi' => $ideal_beauty_multi ? $ideal_beauty_multi : [],
            'best_seller' => $best_seller ? $best_seller : [],
        ];

        return $output;
    }

    private function get_sensor()
    {
        $sensor_basic_model = $this->m_sensor_store_basic_model->get_by_pk(['store_id'=>$this->store_model->id]);
        $sensor_detail_model = $this->m_sensor_store_detail_model->get_by_columns(['sensor_id'=>$sensor_basic_model->id]);

        foreach ($sensor_detail_model as $sensor_detail_row) {
            $sensor[] = [
                'grid_number' => (int)$sensor_detail_row->grid_number,
                'position_row' => (int)$sensor_detail_row->position_row,
                'position_col' => (int)$sensor_detail_row->position_col,
                'group_id' => (int)$sensor_detail_row->group_id,
                'product_id' => (int)$sensor_detail_row->product_id,
                'calibration_min' => (int)$sensor_detail_row->calibration_min,
                'calibration_max' => (int)$sensor_detail_row->calibration_max,
                'threshold_val' => is_null($sensor_detail_row->threshold_val) ? (int)$sensor_detail_row->manual_threshold_val : (int)$sensor_detail_row->threshold_val,
                'enable_flg' => $sensor_detail_row->enable_flg == STATUS_ENABLE ? TRUE : FALSE,
                'address_header' => $sensor_detail_row->address_header,
                'address_index' => (int)$sensor_detail_row->address_index,
            ];
        }

        $output = [
            'grid_row' => (int)$sensor_basic_model->grid_row,
            'grid_col' => (int)$sensor_basic_model->grid_col,
            'sensor' => $sensor,
        ];

        return $output;
    }
}